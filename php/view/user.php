<?php
    function render_file($file) { ?>
        <tr>
            <td><?php echo $file->name; ?></td>
            <td><?php echo $file->upload_time; ?></td>
            <td><a href="?download_file=<?php echo $file->id; ?>"><i class="glyphicon glyphicon-download"></i></a></td>
            <td><a href="?remove_file=<?php echo $file->id; ?>"><i class="glyphicon glyphicon-remove"></i></a></td>
        </tr>
<?php
    } ?>

<?php
    function render_admin_file($file) { ?>
        <tr>
            <td><?php echo $file->name; ?></td>
            <td><?php echo $file->upload_time; ?></td>
            <td><?php echo $file->owner_email; ?></td>
            <td><a href="?download_file=<?php echo $file->id; ?>"><i class="glyphicon glyphicon-download"></i></a></td>
            <td><a href="?remove_file=<?php echo $file->id; ?>"><i class="glyphicon glyphicon-remove"></i></a></td>
        </tr>
<?php
    } ?>

<?php
    function render_note($note) { ?>
        <tr>
            <td><?php echo $note->title; ?></td>
            <td><?php echo $note->upload_time; ?></td>
            <td><a href="view_note.php?note=<?php echo $note->id; ?>"><i class="glyphicon glyphicon-eye-open"></i></a></td>
            <td><a href="?remove_note=<?php echo $note->id; ?>"><i class="glyphicon glyphicon-remove"></i></a></td>
        </tr>
<?php
    } ?>

<?php
    function render_admin_note($note) { ?>
        <tr>
            <td><?php echo $note->title; ?></td>
            <td><?php echo $note->upload_time; ?></td>
            <td><?php echo $note->owner_email; ?></td>
            <td><a href="?remove_note=<?php echo $note->id; ?>"><i class="glyphicon glyphicon-remove"></i></a></td>
        </tr>
<?php
    } ?>

<?php
    function render_user($user) { ?>
        <tr>
            <td><?php echo $user->email; ?></td>
            <td><?php echo $user->username; ?></td>
            <td><?php echo $user->password; ?></td>
            <td><a href="?delete_user=<?php echo $user->id; ?>"><i class="glyphicon glyphicon-remove"></i></a></td>
        </tr>
<?php
    } ?>

<?php
function render_recent_files(array $files) { ?>
    <div class="container">
        <?php if (count($files) > 0) { ?>
            <table class="table table-bordered">
                <thread">
                <tr>
                    <th>File name</th>
                    <th>Upload time</th>
                    <th>Download</th>
                    <th>Remove</th>
                </tr>
                </thread>
                <?php foreach ($files as $file) {
                    render_file($file);
                } ?>
            </table>
        <?php } else { ?>
            <div class="text-center">
                <h3>You haven't uploaded any files recently.</h3>
            </div>
        <?php } ?>
    </div>
    <?php
} ?>

<?php
    function render_recent_notes(array $notes) { ?>
        <div class="container">
            <?php if (count($notes) > 0) { ?>
                <table class="table table-bordered">
                    <thread">
                    <tr>
                        <th>Note title</th>
                        <th>Create time</th>
                        <th>View</th>
                        <th>Remove</th>
                    </tr>
                    </thread>
                    <?php foreach ($notes as $note) {
                        render_note($note);
                    } ?>
                </table>
            <?php } else { ?>
                <div class="text-center">
                    <h3>You haven't created any notes recently.</h3>
                </div>
            <?php } ?>
        </div>
<?php
    } ?>

<?php
    function render_files(array $elements) { ?>
        <div class="container">
            <?php if (count($elements["files"]) > 0) { ?>
                <div class="search-container">
                    <form id="search_form" action="" method="post">
                        <div class="input-group">
                            <input name="input" type="text" class="form-control" placeholder="Search by file name..." />
                            <div class="input-group-btn">
                                <button name="search_button" class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Filter</span>
                    <input id="filter" type="text" class="form-control">
                </div>
                <table class="table table-bordered">
                    <thread>
                    <tr>
                        <th>File name</th>
                        <th>Upload time</th>
                        <th>Download</th>
                        <th>Remove</th>
                    </tr>
                    </thread>
                    <tbody class="searchable">
                        <?php foreach ($elements["files"] as $file) {
                            render_file($file);
                        } ?>
                    </tbody>
                </table>
                <nav>
                    <ul class="pager">
                        <?php if ($elements["current_page"] != 1) { ?>
                            <li><a href="?page=1">First Page</a></li>
                        <?php } ?>
                        <?php if ($elements["previous_page"] != -1) { ?>
                            <li><a href="?page=<?php echo $elements["previous_page"]; ?>">Previous Page</a></li>
                        <?php } ?>
                        <?php if ($elements["next_page"] != -1) { ?>
                            <li><a href="?page=<?php echo $elements["next_page"]; ?>">Next Page</a></li>
                        <?php } ?>
                    </ul>
                </nav>
            <?php } else { ?>
                <div class="text-center">
                    <h3>No files found.</h3>
                </div>
            <?php } ?>
        </div>
<?php
    } ?>

<?php
    function render_admin_files(array $elements) { ?>
        <div class="container">
            <?php if (count($elements["files"]) > 0) { ?>
                <div class="search-container">
                    <form id="search_form" action="" method="post">
                        <div class="input-group">
                            <input name="input" type="text" class="form-control" placeholder="Search by file name..." />
                            <div class="input-group-btn">
                                <button name="search_button" class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Filter</span>
                    <input id="filter" type="text" class="form-control">
                </div>
                <table class="table table-bordered">
                    <thread>
                        <tr>
                            <th>File name</th>
                            <th>Upload time</th>
                            <th>Uploader</th>
                            <th>Download</th>
                            <th>Remove</th>
                        </tr>
                    </thread>
                    <tbody class="searchable">
                    <?php foreach ($elements["files"] as $file) {
                        render_admin_file($file);
                    } ?>
                    </tbody>
                </table>
                <nav>
                    <ul class="pager">
                        <?php if ($elements["current_page"] != 1) { ?>
                            <li><a href="?page=1">First Page</a></li>
                        <?php } ?>
                        <?php if ($elements["previous_page"] != -1) { ?>
                            <li><a href="?page=<?php echo $elements["previous_page"]; ?>">Previous Page</a></li>
                        <?php } ?>
                        <?php if ($elements["next_page"] != -1) { ?>
                            <li><a href="?page=<?php echo $elements["next_page"]; ?>">Next Page</a></li>
                        <?php } ?>
                    </ul>
                </nav>
            <?php } else { ?>
                <div class="text-center">
                    <h3>No files found.</h3>
                </div>
            <?php } ?>
        </div>
<?php
    } ?>

<?php
    function render_notes(array $elements) { ?>
        <div class="container">
            <?php if (count($elements["notes"]) > 0) { ?>
                <div class="search-container">
                    <form id="search_form" action="" method="post">
                        <div class="input-group">
                            <input name="input" type="text" class="form-control" placeholder="Search by note title..." />
                            <div class="input-group-btn">
                                <button name="search_button" class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Filter</span>
                    <input id="filter" type="text" class="form-control">
                </div>
                <table class="table table-bordered">
                    <thread">
                    <tr>
                        <th>Note title</th>
                        <th>Create time</th>
                        <th>View</th>
                        <th>Remove</th>
                    </tr>
                    </thread>
                    <tbody class="searchable">
                        <?php foreach ($elements["notes"] as $note) {
                            render_note($note);
                        } ?>
                    </tbody>
                </table>
                <nav>
                    <ul class="pager">
                        <?php if ($elements["current_page"] != 1) { ?>
                            <li><a href="?page=1">First Page</a></li>
                        <?php } ?>
                        <?php if ($elements["previous_page"] != -1) { ?>
                            <li><a href="?page=<?php echo $elements["previous_page"]; ?>">Previous Page</a></li>
                        <?php } ?>
                        <?php if ($elements["next_page"] != -1) { ?>
                            <li><a href="?page=<?php echo $elements["next_page"]; ?>">Next Page</a></li>
                        <?php } ?>
                    </ul>
                </nav>
            <?php } else { ?>
                <div class="text-center">
                    <h3>No notes found.</h3>
                </div>
            <?php } ?>
        </div>
<?php
    } ?>

<?php
    function render_admin_notes(array $elements) { ?>
        <div class="container">
            <?php if (count($elements["notes"]) > 0) { ?>
                <div class="search-container">
                    <form id="search_form" action="" method="post">
                        <div class="input-group">
                            <input name="input" type="text" class="form-control" placeholder="Search by note title..." />
                            <div class="input-group-btn">
                                <button name="search_button" class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Filter</span>
                    <input id="filter" type="text" class="form-control">
                </div>
                <table class="table table-bordered">
                    <thread">
                    <tr>
                        <th>Note title</th>
                        <th>Create time</th>
                        <th>Creator</th>
                        <th>Remove</th>
                    </tr>
                    </thread>
                    <tbody class="searchable">
                    <?php foreach ($elements["notes"] as $note) {
                        render_admin_note($note);
                    } ?>
                    </tbody>
                </table>
                <nav>
                    <ul class="pager">
                        <?php if ($elements["current_page"] != 1) { ?>
                            <li><a href="?page=1">First Page</a></li>
                        <?php } ?>
                        <?php if ($elements["previous_page"] != -1) { ?>
                            <li><a href="?page=<?php echo $elements["previous_page"]; ?>">Previous Page</a></li>
                        <?php } ?>
                        <?php if ($elements["next_page"] != -1) { ?>
                            <li><a href="?page=<?php echo $elements["next_page"]; ?>">Next Page</a></li>
                        <?php } ?>
                    </ul>
                </nav>
            <?php } else { ?>
                <div class="text-center">
                    <h3>No notes found.</h3>
                </div>
            <?php } ?>
        </div>
<?php
    } ?>

<?php
    function render_users(array $elements) { ?>
        <div class="container">
            <?php if (count($elements["users"]) > 0) { ?>
                <div class="search-container">
                    <form id="search_form" action="" method="post">
                        <div class="input-group">
                            <input name="input" type="text" class="form-control" placeholder="Search by email..." />
                            <div class="input-group-btn">
                                <button name="search_button" class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Filter</span>
                    <input id="filter" type="text" class="form-control">
                </div>
                <table class="table table-bordered">
                    <thread>
                        <tr>
                            <th>Email</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Remove</th>
                        </tr>
                    </thread>
                    <tbody class="searchable">
                    <?php foreach ($elements["users"] as $user) {
                        render_user($user);
                    } ?>
                    </tbody>
                </table>
                <nav>
                    <ul class="pager">
                        <?php if ($elements["current_page"] != 1) { ?>
                            <li><a href="?page=1">First Page</a></li>
                        <?php } ?>
                        <?php if ($elements["previous_page"] != -1) { ?>
                            <li><a href="?page=<?php echo $elements["previous_page"]; ?>">Previous Page</a></li>
                        <?php } ?>
                        <?php if ($elements["next_page"] != -1) { ?>
                            <li><a href="?page=<?php echo $elements["next_page"]; ?>">Next Page</a></li>
                        <?php } ?>
                    </ul>
                </nav>
            <?php } else { ?>
                <div class="text-center">
                    <h3>No users found.</h3>
                </div>
            <?php } ?>
        </div>
<?php
    } ?>

<?php
function render_note_view($note) { ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-10 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-1">
                <form id="note_form" action="user_home.php" method="post">
                    <div class="form-group">
                        <label>Title</label>
                        <input name="title" type="text" class="form-control" value="<?php echo $note->title; ?>" />
                    </div>
                    <div class="form-group">
                        <label>Text</label>
                        <textarea name="text" type="text" rows="10" class="form-control"><?php echo $note->text; ?></textarea>
                    </div>
                    <input name="id" type="hidden" value="<?php echo $note->id; ?>">
                    <div class="form-group">
                        <button name="save_note_button" type="submit" class="btn btn-success btn-block">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
} ?>
