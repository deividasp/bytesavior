<?php
    function load_head($title) { ?>
        <head>
            <title><?php echo $title; ?></title>

            <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/flatly/bootstrap.min.css" rel="stylesheet" />
            <link href="https://fonts.googleapis.com/css?family=Hammersmith+One" rel="stylesheet" />
            <link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />

            <link href="/css/bytesavior.css" rel="stylesheet" />
        </head>
<?php
    } ?>