<?php
    function render_intro($title, $text) { ?>
        <div id="intro">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <img class="img-responsive" src="/img/intro_logo.png" />
                        <span id="intro-title"><?php echo $title; ?></span>
                        <hr class="line-light" />
                        <span id="intro-text"><?php echo $text; ?></span>
                    </div>
                </div>
            </div>
        </div>
<?php
    } ?>

<?php
    function render_info($title, $text, $text_centered = false) { ?>
        <div class="container info">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">
                        <span class="info-title"><?php echo $title; ?></span>
                        <hr class="line-dark" />

                    <?php if ($text_centered) { ?>
                        <span class="info-text"><?php echo $text; ?></span>
                    <?php } ?>

                    </div>

                <?php if (!$text_centered) { ?>
                    <span class="info-text"><?php echo $text; ?></span>
                <?php } ?>

                </div>
            </div>
        </div>
<?php
    } ?>
