<?php
    function load_scripts() { ?>
        <script src="http://code.jquery.com/jquery-2.2.2.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<?php
    } ?>

<?php
    function load_input_validation_scripts() { ?>
        <script src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>
        <script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
        <script src="/js/input_validator.js"></script>
<?php
    } ?>

<?php
    function load_filtering_script() { ?>
        <script src="/js/filterer.js"></script>
<?php
    } ?>

