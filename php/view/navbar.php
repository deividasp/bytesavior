<?php
    require "../model/navbar_element.php";
?>

<?php
    function render_navbar_element($navbar_element) { ?>
        <li><a href="<?php echo $navbar_element->link; ?>"><?php echo $navbar_element->name; ?></a></li>
<?php
    } ?>

<?php
function render_navbar_space() { ?>
        <li class="navbar-space"></li>
    <?php
} ?>

<?php
    function render_navbar(array $navbar_elements) { ?>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">
                        <img src="/img/logo.png" />
                    </a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav" aria-expanded="false">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="nav" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">

                    <?php foreach ($navbar_elements as $navbar_element) {
                        if ($navbar_element->name == "") {
                            render_navbar_space();
                            continue;
                        }

                        render_navbar_element($navbar_element);
                    } ?>

                    </ul>
                </div>
            </div>
        </nav>
<?php
    } ?>
