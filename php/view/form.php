<?php
    require "../model/input_form_group.php";
?>

<?php
    function render_input_form_group($input_form_group) { ?>
        <div class="form-group">
            <label><?php echo $input_form_group->labelText; ?></label>
            <input name="<?php echo $input_form_group->fieldName; ?>" type="<?php echo $input_form_group->fieldType; ?>" class="form-control" />
        </div>
<?php
    } ?>

<?php
    function render_input_form_error() { ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

            <?php if ($_GET["error"] == "incorrect_email") { ?>
                <span>The entered email does not exist.</span>
            <?php } ?>

            <?php if ($_GET["error"] == "incorrect_password") { ?>
                <span>The entered password is incorrect.</span>
            <?php } ?>

        </div>
<?php
    } ?>


<?php
    function render_input_form($id, $action, $buttonName, $buttonText, array $input_form_groups) { ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-10 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-1">
                    <form id="<?php echo $id; ?>" action="<?php echo $action; ?>" method="post">
                        <?php if (isset($_GET["error"])) {
                            render_input_form_error();
                        } ?>

                        <?php foreach ($input_form_groups as $input_form_group) {
                            render_input_form_group($input_form_group);
                        } ?>

                        <div class="form-group">
                            <button name="<?php echo $buttonName; ?>" type="submit" class="btn btn-success btn-block"><?php echo $buttonText; ?></button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
<?php
    } ?>

<?php
    function render_file_form() { ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-10 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-1">
                    <form id="upload_form" action="upload_file.php" method="post" enctype="multipart/form-data">
                        <div class="input-group">
                            <input name="uploaded_file" type="file" />
                        </div>
                        <div class="form-group">
                            <button name="upload_button" type="submit" class="btn btn-success btn-block">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
<?php
    } ?>

<?php
    function render_note_form() { ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-10 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-1">
                    <form id="note_form" action="create_note.php" method="post">
                        <div class="form-group">
                            <label>Title</label>
                            <input name="title" type="text" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Text</label>
                            <textarea name="text" type="text" rows="10" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <button name="create_button" type="submit" class="btn btn-success btn-block">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
<?php
    } ?>