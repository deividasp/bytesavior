<?php
    function check_login() {
        session_start();

        if (isset($_SESSION["username"])) {
            if ($_SESSION["rights"] != 3) {
                header("Location: user_home.php");
            } else {
                header("Location: admin_panel.php");
            }
            exit;
        }

        if (!isset($_POST["login_button"])) {
            return;
        }

        $mysqli = connect();

        $email = $_POST["email"];
        $password = $_POST["password"];

        $query = "SELECT * FROM users WHERE email='$email'";
        $result = query($mysqli, $query);

        if ($result->num_rows == 0) {
            header("Location: login.php?error=incorrect_email");
            exit;
        }

        $rows = $result->fetch_array(MYSQLI_ASSOC);

        if ($password != $rows["password"]) {
            header("Location: login.php?error=incorrect_password");
            exit;
        }

        $mysqli->close();

        $_SESSION["email"] = $rows["email"];
        $_SESSION["username"] = $rows["username"];
        $_SESSION["password"] = $rows["password"];
        $_SESSION["rights"] = $rows["rights"];
        $_SESSION["last_activity"] = time();

        if ($_SESSION["rights"] == 3) {
            header("Location: admin_panel.php");
        } else {
            header("Location: user_home.php");
        }
        exit;
    }

    function check_redirect() {
        session_start();

        if (!isset($_SESSION["username"])) {
            header("Location: index.php");
            exit;
        }

        $TIMEOUT = 600;

        if (isset($_SESSION["last_activity"]) && (time() - $_SESSION["last_activity"] > $TIMEOUT)) {
            session_destroy();
            header("Location: index.php");
            exit;
        }

        $_SESSION["last_activity"] = time();
    }

    function logout() {
        session_start();
        session_destroy();
    }