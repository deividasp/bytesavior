<?php
    require "../model/file.php";
    require "../model/note.php";
    require "../model/user.php";

    function check_premium_rights() {
        if ($_SESSION["rights"] != 2) {
            header("Location: user_home.php");
            exit;
        }
    }

    function check_admin_rights() {
        if ($_SESSION["rights"] != 3) {
            header("Location: user_home.php");
            exit;
        }
    }

    function check_file_upload() {
        if (!isset($_POST["upload_button"])) {
            return;
        }

        $file_name = $_FILES["uploaded_file"]["name"];
        $upload_dir = "../../uploads/" . $_SESSION["email"] . "/";
        $target_path = $upload_dir . basename($_FILES["uploaded_file"]["name"]);

        if (!file_exists($upload_dir)) {
            mkdir($upload_dir);
        }

        if (move_uploaded_file($_FILES["uploaded_file"]["tmp_name"], $target_path)) {
            $mysqli = connect();

            $time = new DateTime();
            $current_time = $time->format("Y-m-d H:i:s");

            $email = $_SESSION["email"];
            $result = query($mysqli, "SELECT * FROM users WHERE email='$email'");
            $rows = $result->fetch_array(MYSQLI_ASSOC);
            $user_id = $rows["id"];

            $result = query($mysqli, "SELECT * FROM files WHERE file_name='$file_name' AND owner='$user_id'");

            if ($result->num_rows > 0) {
                query($mysqli, "UPDATE files SET upload_time='$current_time' WHERE file_name='$file_name' AND owner='$user_id'");
            } else {
                query($mysqli, "INSERT INTO files (file_name, upload_time, owner) VALUES ('$file_name', '$current_time', '$user_id')");
            }

            $mysqli->close();

            header("Location: user_home.php");
        } else {
            header("Location: error.php?error=upload_error");
        }

        exit;
    }

    function check_file_download() {
        if (!isset($_GET["download_file"])) {
            return;
        }

        $mysqli = connect();

        $email = $_SESSION["email"];
        $user_result = query($mysqli, "SELECT * FROM users WHERE email='$email'");
        $user_result_row = $user_result->fetch_array(MYSQLI_ASSOC);
        $user_id = $user_result_row["id"];

        $file_id = $_GET["download_file"];
        $file_result = query($mysqli, "SELECT * FROM files WHERE id='$file_id' AND owner='$user_id'");

        if ($file_result->num_rows > 0) {
            $row = $file_result->fetch_array(MYSQLI_ASSOC);

            $file_name = $row["file_name"];
            $file_path = "../../uploads/" . $_SESSION["email"] . "/" . $file_name;

            header("Content-type: application-x/force-download");
            header("Content-disposition: attachment;filename='$file_name'");
            header("Content-length: " . filesize($file_path));

            ob_clean();
            flush();
            readfile($file_path);
        }
    }

    function check_admin_file_download() {
        if (!isset($_GET["download_file"])) {
            return;
        }

        $mysqli = connect();

        $file_id = $_GET["download_file"];
        $file_result = query($mysqli, "SELECT * FROM files WHERE id='$file_id'");
        $file_result_row = $file_result->fetch_array(MYSQLI_ASSOC);
        $owner_id = $file_result_row["owner"];

        $user_result = query($mysqli, "SELECT * FROM users WHERE id='$owner_id'");
        $user_result_row = $user_result->fetch_array(MYSQLI_ASSOC);

        if ($file_result->num_rows > 0) {
            $file_name = $file_result_row["file_name"];
            $file_path = "../../uploads/" . $user_result_row["email"] . "/" . $file_name;

            header("Content-type: application-x/force-download");
            header("Content-disposition: attachment;filename='$file_name'");
            header("Content-length: " . filesize($file_path));

            ob_clean();
            flush();
            readfile($file_path);
        }
    }

    function check_admin_file_remove() {
        if (!isset($_GET["remove_file"])) {
            return;
        }

        $mysqli = connect();

        $file_id = $_GET["remove_file"];
        $file_result = query($mysqli, "SELECT * FROM files WHERE id='$file_id'");
        $file_result_row = $file_result->fetch_array(MYSQLI_ASSOC);
        $owner_id = $file_result_row["owner"];

        $user_result = query($mysqli, "SELECT * FROM users WHERE id='$owner_id'");
        $user_result_row = $user_result->fetch_array(MYSQLI_ASSOC);

        if ($file_result->num_rows > 0) {
            $file_name = $file_result_row["file_name"];
            $file_path = "../../uploads/" . $user_result_row["email"] . "/" . $file_name;
            unlink($file_path);

            $query = "DELETE FROM files WHERE id='$file_id'";
            query($mysqli, $query);

            $mysqli->close();
        }
    }

    function check_file_remove() {
        if (!isset($_GET["remove_file"])) {
            return;
        }

        $mysqli = connect();

        $email = $_SESSION["email"];
        $user_result = query($mysqli, "SELECT * FROM users WHERE email='$email'");
        $user_result_row = $user_result->fetch_array(MYSQLI_ASSOC);
        $user_id = $user_result_row["id"];

        $file_id = $_GET["remove_file"];
        $file_result = query($mysqli, "SELECT * FROM files WHERE id='$file_id' AND owner='$user_id'");

        if ($file_result->num_rows > 0) {
            $row = $file_result->fetch_array(MYSQLI_ASSOC);

            $file_name = $row["file_name"];
            $file_path = "../../uploads/" . $_SESSION["email"] . "/" . $file_name;
            unlink($file_path);

            $query = "DELETE FROM files WHERE id='$file_id'";

            query($mysqli, $query);

            $mysqli->close();
        }
    }

    function check_note_create() {
        if (!isset($_POST["create_button"])) {
            return;
        }

        $title = $_POST["title"];
        $text = $_POST["text"];

        $mysqli = connect();

        $time = new DateTime();
        $current_time = $time->format("Y-m-d H:i:s");

        $email = $_SESSION["email"];
        $result = query($mysqli, "SELECT * FROM users WHERE email='$email'");
        $rows = $result->fetch_array(MYSQLI_ASSOC);
        $user_id = $rows["id"];

        $query = "INSERT INTO notes (title, text, create_time, owner) VALUES ('$title', '$text', '$current_time', '$user_id')";

        query($mysqli, $query);

        $mysqli->close();

        header("Location: user_home.php");
    }

    function check_note_remove() {
        if (!isset($_GET["remove_note"])) {
            return;
        }

        $mysqli = connect();

        $email = $_SESSION["email"];
        $user_result = query($mysqli, "SELECT * FROM users WHERE email='$email'");
        $user_result_row = $user_result->fetch_array(MYSQLI_ASSOC);
        $user_id = $user_result_row["id"];

        $note_id = $_GET["remove_note"];
        query($mysqli, "DELETE FROM notes WHERE id='$note_id' AND owner='$user_id'");

        $mysqli->close();
    }

    function check_admin_note_remove() {
        if (!isset($_GET["remove_note"])) {
            return;
        }

        $mysqli = connect();

        $note_id = $_GET["remove_note"];
        $note_result = query($mysqli, "SELECT * FROM notes WHERE id='$note_id'");

        if ($note_result->num_rows > 0) {
            $query = "DELETE FROM notes WHERE id='$note_id'";
            query($mysqli, $query);

            $mysqli->close();
        }
    }

    function check_user_delete() {
        if (!isset($_GET["delete_user"])) {
            return;
        }

        $mysqli = connect();

        $user_id = $_GET["delete_user"];
        $user_result = query($mysqli, "SELECT * FROM users WHERE id='$user_id'");
        $user_result_row = $user_result->fetch_array(MYSQLI_ASSOC);

        if ($user_result->num_rows > 0) {
            $query = "DELETE FROM users WHERE id='$user_id'";
            query($mysqli, $query);

            $query = "DELETE FROM files WHERE owner='$user_id'";
            query($mysqli, $query);

            $query = "DELETE FROM notes WHERE owner='$user_id'";
            query($mysqli, $query);

            $file_path = "../../uploads/" . $user_result_row["email"] . "/";

            array_map("unlink", glob("$file_path" . "*"));
            rmdir($file_path);
        }

        $mysqli->close();
    }

    function check_note_save() {
        if (!isset($_POST["save_note_button"])) {
            return;
        }

        $mysqli = connect();

        $note_id = $_POST["id"];
        $note_title = $_POST["title"];
        $note_text = $_POST["text"];

        query($mysqli, "UPDATE notes SET title='$note_title', text='$note_text' WHERE id='$note_id'");

        $mysqli->close();
    }

    function get_recent_files() {
        $DISPLAY_LIMIT = 5;

        if (isset($_GET["download_file"]) || isset($_get["remove_file"])) {
            return array();
        }

        $mysqli = connect();

        $email = $_SESSION["email"];
        $user_result = query($mysqli, "SELECT * FROM users WHERE email='$email'");
        $rows = $user_result->fetch_array(MYSQLI_ASSOC);
        $user_id = $rows["id"];

        $file_result = query($mysqli, "SELECT * FROM files WHERE owner='$user_id' ORDER BY upload_time DESC LIMIT $DISPLAY_LIMIT");

        $files = array();

        while ($row = $file_result->fetch_array(MYSQLI_ASSOC)) {
            array_push($files, new file($row["id"], $row["file_name"], $row["upload_time"]));
        }

        return $files;
    }

    function get_recent_notes() {
        $DISPLAY_LIMIT = 5;

        if (isset($_get["remove_note"])) {
            return array();
        }

        $mysqli = connect();

        $email = $_SESSION["email"];
        $user_result = query($mysqli, "SELECT * FROM users WHERE email='$email'");
        $rows = $user_result->fetch_array(MYSQLI_ASSOC);
        $user_id = $rows["id"];

        $note_result = query($mysqli, "SELECT * FROM notes WHERE owner='$user_id' ORDER BY create_time DESC LIMIT $DISPLAY_LIMIT");

        $notes = array();

        while ($row = $note_result->fetch_array(MYSQLI_ASSOC)) {
            array_push($notes, new note($row["id"], $row["title"], $row["text"], $row["create_time"]));
        }

        return $notes;
    }

    function get_files() {
        $FILES_PER_PAGE = 5;

        if (isset($_GET["download_file"])) {
            return array();
        }

        if (!isset($_GET["page"])) {
            $page = 1;
        } else {
            $page = $_GET["page"];
        }

        $current_page_offset = ($page - 1) * $FILES_PER_PAGE;
        $previous_page_offset = ($page - 2) * $FILES_PER_PAGE;
        $next_page_offset = ($page) * $FILES_PER_PAGE;

        $mysqli = connect();

        $email = $_SESSION["email"];
        $user_result = query($mysqli, "SELECT * FROM users WHERE email='$email'");
        $rows = $user_result->fetch_array(MYSQLI_ASSOC);
        $user_id = $rows["id"];

        if (isset($_POST["search_button"]) && strlen($_POST["input"]) > 0) {
            $search_value = $_POST["input"];
            $current_page_file_result = query($mysqli, "SELECT * FROM files WHERE owner='$user_id' AND file_name LIKE '%$search_value%'");
        } else {
            $current_page_file_result = query($mysqli, "SELECT * FROM files WHERE owner='$user_id' ORDER BY upload_time DESC LIMIT $FILES_PER_PAGE OFFSET $current_page_offset");
        }

        $previous_page = -1;
        $next_page = -1;

        if ($previous_page_offset > -1) {
            $previous_page_file_result = query($mysqli, "SELECT * FROM files WHERE owner='$user_id' ORDER BY upload_time DESC LIMIT $FILES_PER_PAGE OFFSET $previous_page_offset");

            if ($previous_page_file_result->num_rows > 0) {
                $previous_page = $page - 1;
            }
        }

        $next_page_file_result = query($mysqli, "SELECT * FROM files WHERE owner='$user_id' ORDER BY upload_time DESC LIMIT $FILES_PER_PAGE OFFSET $next_page_offset");

        if ($next_page_file_result->num_rows > 0) {
            $next_page = $page + 1;
        }

        $files = array();

        while ($row = $current_page_file_result->fetch_array(MYSQLI_ASSOC)) {
            array_push($files, new file($row["id"], $row["file_name"], $row["upload_time"]));
        }

        if (isset($_POST["search_button"]) && strlen($_POST["input"]) > 0) {
            return array (
                "files" => $files,
                "current_page" => 1,
                "previous_page" => -1,
                "next_page" => -1);
        }

        return array (
            "files" => $files,
            "current_page" => $page,
            "previous_page" => $previous_page,
            "next_page" => $next_page);
    }

    function get_all_files() {
        $FILES_PER_PAGE = 5;

        if (isset($_GET["download_file"])) {
            return array();
        }

        if (!isset($_GET["page"])) {
            $page = 1;
        } else {
            $page = $_GET["page"];
        }

        $current_page_offset = ($page - 1) * $FILES_PER_PAGE;
        $previous_page_offset = ($page - 2) * $FILES_PER_PAGE;
        $next_page_offset = ($page) * $FILES_PER_PAGE;

        $mysqli = connect();

        if (isset($_POST["search_button"]) && strlen($_POST["input"]) > 0) {
            $search_value = $_POST["input"];
            $current_page_file_result = query($mysqli, "SELECT * FROM files WHERE file_name LIKE '%$search_value%'");
        } else {
            $current_page_file_result = query($mysqli, "SELECT * FROM files ORDER BY upload_time DESC LIMIT $FILES_PER_PAGE OFFSET $current_page_offset");
        }

        $previous_page = -1;
        $next_page = -1;

        if ($previous_page_offset > -1) {
            $previous_page_file_result = query($mysqli, "SELECT * FROM files ORDER BY upload_time DESC LIMIT $FILES_PER_PAGE OFFSET $previous_page_offset");

            if ($previous_page_file_result->num_rows > 0) {
                $previous_page = $page - 1;
            }
        }

        $next_page_file_result = query($mysqli, "SELECT * FROM files ORDER BY upload_time DESC LIMIT $FILES_PER_PAGE OFFSET $next_page_offset");

        if ($next_page_file_result->num_rows > 0) {
            $next_page = $page + 1;
        }

        $files = array();

        while ($row = $current_page_file_result->fetch_array(MYSQLI_ASSOC)) {
            $file = new file($row["id"], $row["file_name"], $row["upload_time"]);
            $file_owner_id = $row["owner"];

            $user_result = query($mysqli, "SELECT * FROM users WHERE id='$file_owner_id'");
            $row = $user_result->fetch_array(MYSQLI_ASSOC);
            $file->owner_email = $row["email"];

            array_push($files, $file);
        }

        if (isset($_POST["search_button"]) && strlen($_POST["input"]) > 0) {
            return array (
                "files" => $files,
                "current_page" => 1,
                "previous_page" => -1,
                "next_page" => -1);
        }

        return array (
            "files" => $files,
            "current_page" => $page,
            "previous_page" => $previous_page,
            "next_page" => $next_page);
    }

    function get_notes() {
        $NOTES_PER_PAGE = 5;

        if (!isset($_GET["page"])) {
            $page = 1;
        } else {
            $page = $_GET["page"];
        }

        $current_page_offset = ($page - 1) * $NOTES_PER_PAGE;
        $previous_page_offset = ($page - 2) * $NOTES_PER_PAGE;
        $next_page_offset = ($page) * $NOTES_PER_PAGE;

        $mysqli = connect();

        $email = $_SESSION["email"];
        $user_result = query($mysqli, "SELECT * FROM users WHERE email='$email'");
        $rows = $user_result->fetch_array(MYSQLI_ASSOC);
        $user_id = $rows["id"];

        if (isset($_POST["search_button"]) && strlen($_POST["input"]) > 0) {
            $search_value = $_POST["input"];
            $current_page_note_result = query($mysqli, "SELECT * FROM notes WHERE owner='$user_id' AND title LIKE '%$search_value%'");
        } else {
            $current_page_note_result = query($mysqli, "SELECT * FROM notes WHERE owner='$user_id' ORDER BY create_time DESC LIMIT $NOTES_PER_PAGE OFFSET $current_page_offset");
        }

        $previous_page = -1;
        $next_page = -1;

        if ($previous_page_offset > -1) {
            $previous_page_note_result = query($mysqli, "SELECT * FROM notes WHERE owner='$user_id' ORDER BY create_time DESC LIMIT $NOTES_PER_PAGE OFFSET $previous_page_offset");

            if ($previous_page_note_result->num_rows > 0) {
                $previous_page = $page - 1;
            }
        }

        $next_page_note_result = query($mysqli, "SELECT * FROM notes WHERE owner='$user_id' ORDER BY create_time DESC LIMIT $NOTES_PER_PAGE OFFSET $next_page_offset");

        if ($next_page_note_result->num_rows > 0) {
            $next_page = $page + 1;
        }

        $notes = array();

        while ($row = $current_page_note_result->fetch_array(MYSQLI_ASSOC)) {
            array_push($notes, new note($row["id"], $row["title"], $row["text"], $row["create_time"]));
        }

        if (isset($_POST["search_button"]) && strlen($_POST["input"]) > 0) {
            return array (
                "notes" => $notes,
                "current_page" => 1,
                "previous_page" => -1,
                "next_page" => -1);
        }

        return array (
            "notes" => $notes,
            "current_page" => $page,
            "previous_page" => $previous_page,
            "next_page" => $next_page);
    }

    function get_all_notes() {
        $NOTES_PER_PAGE = 5;

        if (!isset($_GET["page"])) {
            $page = 1;
        } else {
            $page = $_GET["page"];
        }

        $current_page_offset = ($page - 1) * $NOTES_PER_PAGE;
        $previous_page_offset = ($page - 2) * $NOTES_PER_PAGE;
        $next_page_offset = ($page) * $NOTES_PER_PAGE;

        $mysqli = connect();

        if (isset($_POST["search_button"]) && strlen($_POST["input"]) > 0) {
            $search_value = $_POST["input"];
            $current_page_note_result = query($mysqli, "SELECT * FROM notes WHERE title LIKE '%$search_value%'");
        } else {
            $current_page_note_result = query($mysqli, "SELECT * FROM notes ORDER BY create_time DESC LIMIT $NOTES_PER_PAGE OFFSET $current_page_offset");
        }

        $previous_page = -1;
        $next_page = -1;

        if ($previous_page_offset > -1) {
            $previous_page_note_result = query($mysqli, "SELECT * FROM notes ORDER BY create_time DESC LIMIT $NOTES_PER_PAGE OFFSET $previous_page_offset");

            if ($previous_page_note_result->num_rows > 0) {
                $previous_page = $page - 1;
            }
        }

        $next_page_note_result = query($mysqli, "SELECT * FROM notes ORDER BY create_time DESC LIMIT $NOTES_PER_PAGE OFFSET $next_page_offset");

        if ($next_page_note_result->num_rows > 0) {
            $next_page = $page + 1;
        }

        $notes = array();

        while ($row = $current_page_note_result->fetch_array(MYSQLI_ASSOC)) {
            $note = new note($row["id"], $row["title"], $row["text"], $row["create_time"]);
            $note_owner_id = $row["owner"];

            $user_result = query($mysqli, "SELECT * FROM users WHERE id='$note_owner_id'");
            $row = $user_result->fetch_array(MYSQLI_ASSOC);
            $note->owner_email = $row["email"];

            array_push($notes, $note);
        }

        if (isset($_POST["search_button"]) && strlen($_POST["input"]) > 0) {
            return array (
                "notes" => $notes,
                "current_page" => 1,
                "previous_page" => -1,
                "next_page" => -1);
        }

        return array (
            "notes" => $notes,
            "current_page" => $page,
            "previous_page" => $previous_page,
            "next_page" => $next_page);
    }

    function get_users() {
        $USERS_PER_PAGE = 5;

        if (!isset($_GET["page"])) {
            $page = 1;
        } else {
            $page = $_GET["page"];
        }

        $current_page_offset = ($page - 1) * $USERS_PER_PAGE;
        $previous_page_offset = ($page - 2) * $USERS_PER_PAGE;
        $next_page_offset = ($page) * $USERS_PER_PAGE;

        $mysqli = connect();

        if (isset($_POST["search_button"]) && strlen($_POST["input"]) > 0) {
            $search_value = $_POST["input"];
            $current_page_note_result = query($mysqli, "SELECT * FROM users WHERE email LIKE '%$search_value%'");
        } else {
            $current_page_note_result = query($mysqli, "SELECT * FROM users ORDER BY email DESC LIMIT $USERS_PER_PAGE OFFSET $current_page_offset");
        }

        $previous_page = -1;
        $next_page = -1;

        if ($previous_page_offset > -1) {
            $previous_page_note_result = query($mysqli, "SELECT * FROM users ORDER BY email DESC LIMIT $USERS_PER_PAGE OFFSET $previous_page_offset");

            if ($previous_page_note_result->num_rows > 0) {
                $previous_page = $page - 1;
            }
        }

        $next_page_note_result = query($mysqli, "SELECT * FROM users ORDER BY email DESC LIMIT $USERS_PER_PAGE OFFSET $next_page_offset");

        if ($next_page_note_result->num_rows > 0) {
            $next_page = $page + 1;
        }

        $users = array();

        while ($row = $current_page_note_result->fetch_array(MYSQLI_ASSOC)) {
            array_push($users, new user($row["id"], $row["email"], $row["username"], $row["password"]));
        }

        if (isset($_POST["search_button"]) && strlen($_POST["input"]) > 0) {
            return array (
                "users" => $users,
                "current_page" => 1,
                "previous_page" => -1,
                "next_page" => -1);
        }

        return array (
            "users" => $users,
            "current_page" => $page,
            "previous_page" => $previous_page,
            "next_page" => $next_page);
    }

    function get_note() {
        $note_id = $_GET["note"];

        $mysqli = connect();

        $email = $_SESSION["email"];
        $user_result = query($mysqli, "SELECT * FROM users WHERE email='$email'");
        $rows = $user_result->fetch_array(MYSQLI_ASSOC);
        $user_id = $rows["id"];

        $note_result = query($mysqli, "SELECT * FROM notes WHERE owner='$user_id' AND id='$note_id'");

        if ($note_result->num_rows > 0) {
            $row = $note_result->fetch_array(MYSQLI_ASSOC);
            return new note($row["id"], $row["title"], $row["text"], $row["create_time"]);
        } else {
            return null;
        }
    }

    function check_profile_update() {
        if (isset($_POST["profile_update_button"])) {
            $mysqli = connect();

            $old_username = $_SESSION["username"];
            $new_username = $_POST["username"];
            $new_password = $_POST["password"];

            if (strlen($new_username) > 0) {
                query($mysqli, "UPDATE users SET username='$new_username' WHERE username='$old_username'");
                $_SESSION["username"] = $new_username;
            }

            if (strlen($new_password) > 0) {
                query($mysqli, "UPDATE users SET password='$new_password' WHERE username='$old_username'");
                $_SESSION["password"] = $new_password;
            }

            $mysqli->close();
        }
    }