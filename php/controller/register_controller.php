<?php
    function check_register() {
        if (!isset($_POST["register_button"])) {
            return;
        }

        $mysqli = connect();

        $email = $_POST["email"];

        $query = "SELECT * FROM users WHERE email='$email'";
        $result = query($mysqli, $query);

        if ($result->num_rows > 0) {
            header("Location: error.php?error=register_email_already_exists");
            exit;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            header("Location: error.php?error=register_invalid_email");
            exit;
        }

        $username = $_POST["username"];

        if (strlen($username) < 1 || strlen($username) > 15 || !preg_match("/^[a-zA-Z0-9]+$/", $username)) {
            header("Location: error?php?error=register_invalid_username");
            exit;
        }

        $password = $_POST["password"];

        if (strlen($password) < 1) {
            header("Location: error?php?error=register_invalid_password");
            exit;
        }

        $query = "INSERT INTO users (email, username, password, rights) VALUES ('$email', '$username', '$password', 1)";

        query($mysqli, $query);

        $mysqli->close();

        header("Location: register_success.php");
    }