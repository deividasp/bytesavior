<?php
    define("DB_HOST", "host");
    define("DB_NAME", "name");
    define("DB_USER", "user");
    define("DB_PASSWORD", "password");

    function connect() {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

        if ($mysqli->connect_error) {
            session_start();
            session_destroy();

            header("Location: error.php?error=db_connect_error");
            exit;
        }

        return $mysqli;
    }

    function query($mysqli, $query) {
        if (($result = $mysqli->query($query)) === FALSE) {
            header("Location: error.php?error=db_query_error_" . $mysqli->errno);
            exit;
        }

        return $result;
    }