<?php
    class user {

        public $id;
        public $email;
        public $username;
        public $password;

        function __construct($id, $email, $username, $password) {
            $this->id = $id;
            $this->email = $email;
            $this->username = $username;
            $this->password = $password;
        }

    }