<?php
    class note {

        public $id;
        public $title;
        public $text;
        public $upload_time;
        public $owner_email;

        function __construct($id, $title, $text, $upload_time) {
            $this->id = $id;
            $this->title = $title;
            $this->text = $text;
            $this->upload_time = $upload_time;
        }

    }