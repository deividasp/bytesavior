<?php
    class file {

        public $id;
        public $name;
        public $upload_time;
        public $owner_email;

        function __construct($id, $name, $upload_time) {
            $this->id = $id;
            $this->name = $name;
            $this->upload_time = $upload_time;
        }

    }