<?php
    class input_form_group {

        public $labelText;
        public $fieldName;
        public $fieldType;

        public function __construct($labelText, $fieldName, $fieldType) {
            $this->labelText = $labelText;
            $this->fieldName = $fieldName;
            $this->fieldType = $fieldType;
        }

    }