<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_login();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - Index");
    ?>

    <body>
        <?php
            render_navbar(array(
                new navbar_element("Login", "login.php"),
                new navbar_element("Register", "register.php")
            ));

            render_intro("ByteSavior", "Safe - Powerful - Easy to use");

            render_info("About",
                "ByteSavior is a free, safe, powerful and easy to use personal data storage solution made by Deividas
                 Popelskis for Kaunas University of Technology module T120B145 (Computer Networks and Internet Technologies).");

            load_scripts();
        ?>
    </body>
</html>
