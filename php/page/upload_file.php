<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_redirect();
    check_file_upload();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - Upload File");
    ?>

        <body>
            <?php
            if ($_SESSION["rights"] == 2) {
                render_navbar(array(
                    new navbar_element("Create note", "create_note.php"),
                    new navbar_element("View notes", "view_notes.php"),
                    new navbar_element("", ""),
                    new navbar_element("View files", "view_files.php"),
                    new navbar_element("", ""),
                    new navbar_element("Home", "user_home.php"),
                    new navbar_element("Logout", "logout.php")
                ));
            } else {
                render_navbar(array(
                    new navbar_element("View files", "view_files.php"),
                    new navbar_element("", ""),
                    new navbar_element("Home", "user_home.php"),
                    new navbar_element("Logout", "logout.php")
                ));
            }

            render_info("Upload", "Select a file you want to upload.", true);

            render_file_form();

            load_scripts();
            load_input_validation_scripts();
        ?>
    </body>
</html>
