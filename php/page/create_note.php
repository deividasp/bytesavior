<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_redirect();
    check_premium_rights();
    check_note_create();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - Create Note");
    ?>

    <body>
        <?php
            render_navbar(array(
                new navbar_element("View notes", "view_notes.php"),
                new navbar_element("", ""),
                new navbar_element("Upload file", "upload_file.php"),
                new navbar_element("View files", "view_files.php"),
                new navbar_element("", ""),
                new navbar_element("Home", "user_home.php"),
                new navbar_element("Logout", "logout.php")
            ));

            render_info("Create", "Fill the fields below to create a new note.", true);

            render_note_form();

            load_scripts();
            load_input_validation_scripts();
        ?>
    </body>
</html>
