<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_redirect();
    check_admin_rights();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - Admin Panel");
    ?>

    <body>
        <?php
            render_navbar(array(
                new navbar_element("Manage Users", "admin_user_management.php"),
                new navbar_element("Manage Files", "admin_file_management.php"),
                new navbar_element("Manage Notes", "admin_note_management.php"),
                new navbar_element("", ""),
                new navbar_element("Logout", "logout.php")
            ));

            render_info("Admin Panel", "Welcome to admin panel.
                                        <br>
                                        <br>
                                        Please select what you wish to manage in the navigation bar above.", true);

            load_scripts();
        ?>
    </body>
</html>
