<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_redirect();
    check_admin_rights();
    check_user_delete();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - User Management");
    ?>

    <body>
        <?php
            render_navbar(array(
                new navbar_element("Manage Files", "admin_file_management.php"),
                new navbar_element("Manage Notes", "admin_note_management.php"),
                new navbar_element("", ""),
                new navbar_element("Home", "admin_panel.php"),
                new navbar_element("Logout", "logout.php")
            ));

            render_info("Users", "", true);

            render_users(get_users());

            load_scripts();
            load_filtering_script();
        ?>
    </body>
</html>
