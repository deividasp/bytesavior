<?php
    require "../view_core.php";
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - Error");
    ?>

    <body>
        <?php
            render_navbar(array(
                new navbar_element("Index", "index.php")
            ));

            render_info("An error has occurred", "Error code: " . $_GET["error"], true);

            load_scripts();
        ?>
    </body>
</html>
