<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_login();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - Register");
    ?>

    <body>
        <?php
            render_navbar(array(
                new navbar_element("Index", "index.php"),
                new navbar_element("Register", "register.php")
            ));

            render_info("Login", "Enter the required information below to login into an existing account.", true);

            render_input_form("login_form", "login.php", "login_button", "Login", array(
                new input_form_group("Email address", "email", "email"),
                new input_form_group("Password", "password", "password")));

            load_scripts();
            load_input_validation_scripts();
        ?>
    </body>
</html>
