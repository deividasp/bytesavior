<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_redirect();
    check_admin_rights();
    check_admin_file_download();
    check_admin_file_remove();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - File Management");
    ?>

    <body>
        <?php
            render_navbar(array(

                new navbar_element("Manage Users", "admin_user_management.php"),
                new navbar_element("Manage Notes", "admin_note_management.php"),
                new navbar_element("", ""),
                new navbar_element("Home", "admin_panel.php"),
                new navbar_element("Logout", "logout.php")
            ));

            render_info("Files", "", true);

            render_admin_files(get_all_files());

            load_scripts();
            load_filtering_script();
        ?>
    </body>
</html>
