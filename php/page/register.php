<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_register();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - Register");
    ?>

    <body>
        <?php
            render_navbar(array(
                new navbar_element("Index", "index.php"),
                new navbar_element("Login", "login.php")
            ));

            render_info("Register", "Enter the required information below to register a new account.", true);

            render_input_form("register_form", "register.php", "register_button", "Register", array(
                new input_form_group("Email address", "email", "email"),
                new input_form_group("Username", "username", "text"),
                new input_form_group("Password", "password", "password"),
                new input_form_group("Password (confirm)", "password_confirm", "password")));

            load_scripts();
            load_input_validation_scripts();
        ?>
    </body>
</html>
