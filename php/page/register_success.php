<?php
    require "../view_core.php";
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - Success");
    ?>

        <body>
            <?php
            render_navbar(array(
                new navbar_element("Index", "index.php"),
                new navbar_element("Login", "login.php")
            ));

            render_info("Success", "Your registration is successful. You can now login.", true);

            load_scripts();
        ?>
    </body>
</html>
