<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_redirect();
    check_premium_rights();
    check_note_remove();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - " . $_SESSION["username"]);
    ?>

    <body>
        <?php
            render_navbar(array(
                new navbar_element("Create note", "create_note.php"),
                new navbar_element("", ""),
                new navbar_element("Upload file", "upload_file.php"),
                new navbar_element("View files", "view_files.php"),
                new navbar_element("", ""),
                new navbar_element("Home", "user_home.php"),
                new navbar_element("Logout", "logout.php")
            ));

            render_info("Notes", "Your created notes are shown below.", true);

            render_notes(get_notes());

            load_scripts();
            load_filtering_script();
        ?>
    </body>
</html>
