<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_redirect();
    check_file_download();
    check_file_remove();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - View Files");
    ?>

    <body>
        <?php
            if ($_SESSION["rights"] == 2) {
                render_navbar(array(
                    new navbar_element("Create note", "create_note.php"),
                    new navbar_element("View notes", "view_notes.php"),
                    new navbar_element("", ""),
                    new navbar_element("Upload file", "upload_file.php"),
                    new navbar_element("", ""),
                    new navbar_element("Home", "user_home.php"),
                    new navbar_element("Logout", "logout.php")
                ));
            } else {
                render_navbar(array(
                    new navbar_element("Upload file", "upload_file.php"),
                    new navbar_element("", ""),
                    new navbar_element("Home", "user_home.php"),
                    new navbar_element("Logout", "logout.php")
                ));
            }

            render_info("Files", "Your uploaded files are shown below.", true);

            render_files(get_files());

            load_scripts();
            load_filtering_script();
        ?>
    </body>
</html>
