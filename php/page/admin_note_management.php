<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_redirect();
    check_admin_rights();
    check_admin_note_remove();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - Note Management");
    ?>

    <body>
        <?php
            render_navbar(array(

                new navbar_element("Manage Users", "admin_user_management.php"),
                new navbar_element("Manage Files", "admin_file_management.php"),
                new navbar_element("", ""),
                new navbar_element("Home", "admin_panel.php"),
                new navbar_element("Logout", "logout.php")
            ));

            render_info("Notes", "", true);

            render_admin_notes(get_all_notes());

            load_scripts();
            load_filtering_script();
        ?>
    </body>
</html>
