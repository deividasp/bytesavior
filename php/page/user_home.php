<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_redirect();
    check_profile_update();
    check_file_download();
    check_file_remove();
    check_note_remove();
    check_note_save();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - " . $_SESSION["username"]);
    ?>

    <body>
        <?php
            if ($_SESSION["rights"] == 2) {
                render_navbar(array(
                    new navbar_element("Create note", "create_note.php"),
                    new navbar_element("View notes", "view_notes.php"),
                    new navbar_element("", ""),
                    new navbar_element("Upload file", "upload_file.php"),
                    new navbar_element("View files", "view_files.php"),
                    new navbar_element("", ""),
                    new navbar_element("Profile", "user_profile.php"),
                    new navbar_element("Logout", "logout.php")
                ));
            } else {
                render_navbar(array(
                    new navbar_element("Upload file", "upload_file.php"),
                    new navbar_element("View files", "view_files.php"),
                    new navbar_element("", ""),
                    new navbar_element("Profile", "user_profile.php"),
                    new navbar_element("Logout", "logout.php")
                ));
            }

            render_info("Files", "Your recently uploaded files are shown below.", true);
            render_recent_files(get_recent_files());


            if ($_SESSION["rights"] == 2) {
                render_info("Notes", "Your recently uploaded notes are shown below.", true);
                render_recent_notes(get_recent_notes());
            }

            load_scripts();
        ?>
    </body>
</html>
