<?php
    require "../view_core.php";
    require "../controller_core.php";

    check_redirect();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - Profile");
    ?>

    <body>
        <?php
            if ($_SESSION["rights"] == 2) {
                render_navbar(array(
                    new navbar_element("Create note", "create_note.php"),
                    new navbar_element("View notes", "view_notes.php"),
                    new navbar_element("", ""),
                    new navbar_element("Upload file", "upload_file.php"),
                    new navbar_element("View files", "view_files.php"),
                    new navbar_element("", ""),
                    new navbar_element("Home", "user_home.php"),
                    new navbar_element("Logout", "logout.php")
                ));
            } else {
                render_navbar(array(
                    new navbar_element("Upload file", "upload_file.php"),
                    new navbar_element("View files", "view_files.php"),
                    new navbar_element("", ""),
                    new navbar_element("Home", "user_home.php"),
                    new navbar_element("Logout", "logout.php")
                ));
            }

            render_info("Profile", "Update your current profile information by filling the fields below.", true);

            render_input_form("profile_form", "user_home.php", "profile_update_button", "Update", array(
                new input_form_group("Username", "username", "username"),
                new input_form_group("Password", "password", "password"),
                new input_form_group("Password (confirm)", "password_confirm", "password")));

            load_scripts();
            load_input_validation_scripts();
        ?>
    </body>
</html>
