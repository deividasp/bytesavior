<?php
    require "../view_core.php";
    require "../controller_core.php";

    logout();
?>

<!DOCTYPE html>
<html>
    <?php
        load_head("ByteSavior - Logout");
    ?>

    <body>
        <?php
            render_navbar(array(
                new navbar_element("Login", "login.php"),
                new navbar_element("Register", "register.php")
            ));

            render_info("Logout", "You have logged out.", true);

            load_scripts();
        ?>
    </body>
</html>
