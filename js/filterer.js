$(document).ready(function() {

    (function($) {

        $("#filter").keyup(function() {
            var regex = new RegExp($(this).val(), "i");

            $(".searchable tr").hide();
            $(".searchable tr").filter(function() {
                return regex.test($(this).text());
            }).show();
        })

    }(jQuery))
})