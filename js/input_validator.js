$("#register_form").formValidation( {
    framework: "bootstrap",

    icon: {
        valid: "glyphicon glyphicon-ok",
        invalid: "glyphicon glyphicon-remove"
    },

    fields: {
        email: {
            validators: {
                notEmpty: {
                    message: "Email address is required."
                },
                emailAddress: {
                    message: "Invalid email address."
                },
                remote: {
                    url: "/php/controller/email_validator.php",
                    type: "POST",
                    delay: 1000,
                    message: "This email address is already in use.",
                }
            }
        },
        username: {
            validators: {
                notEmpty: {
                    message: "Username is required."
                },
                stringLength: {
                    max: 15,
                    message: "Username must contain less than 16 characters."
                },
                regexp: {
                    regexp: /^[a-zA-Z0-9]+$/,
                    message: "Username contains illegal symbols, only letters and numbers allowed."
                }
            }
        },
        password: {
            validators: {
                notEmpty: {
                    message: "Password is required."
                }
            }
        },
        password_confirm: {
            validators: {
                notEmpty: {
                    message: "Password confirmation is required."
                },
                identical: {
                    field: "password",
                    message: "Confirmed password does not match the original one."
                }
            }
        }
    }
});

$("#login_form").formValidation( {
    framework: "bootstrap",

    icon: {
        valid: "glyphicon glyphicon-ok",
        invalid: "glyphicon glyphicon-remove"
    },

    fields: {
        email: {
            validators: {
                notEmpty: {
                    message: "Email address is required."
                },
                emailAddress: {
                    message: "Invalid email address."
                }
            }
        },
        password: {
            validators: {
                notEmpty: {
                    message: "Password is required."
                }
            }
        }
    }
});

$("#note_form").formValidation( {
    framework: "bootstrap",

    icon: {
        valid: "glyphicon glyphicon-ok",
        invalid: "glyphicon glyphicon-remove"
    },

    fields: {
        title: {
            validators: {
                notEmpty: {
                    message: "Please enter the note title."
                }
            }
        },
        text: {
            validators: {
                notEmpty: {
                    message: "Please enter the note text."
                }
            }
        }
    }
});

$("#profile_form").formValidation( {
    framework: "bootstrap",

    icon: {
        valid: "glyphicon glyphicon-ok",
        invalid: "glyphicon glyphicon-remove"
    },

    fields: {
        password_confirm: {
            validators: {
                identical: {
                    field: "password",
                    message: "Confirmed password does not match the original one."
                }
            }
        }
    }
});

$("#upload_form").formValidation( {
    framework: "bootstrap",

    icon: {
        valid: "glyphicon glyphicon-ok",
        invalid: "glyphicon glyphicon-remove"
    },

    fields: {
        uploaded_file: {
            validators: {
                notEmpty: {
                    message: "No file selected."
                }
            }
        }
    }
});